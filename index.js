var through = require('through2');
var fs = require('fs');
var wsuduBuilder = require("wsudu-builder");

function wsudu(userCfg) {
    return through.obj(function (file, enc, cb) {

        var cfg = {
            callback: function () {
                cb(null, file);
            }
        }

        cfg = newItem = Object.assign({}, cfg, userCfg);
        wsuduBuilder(file.contents.toString("utf8"), cfg);
    });
}

module.exports = wsudu;
